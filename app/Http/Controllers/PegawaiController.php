<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Profile;
use App\Golongan;
use App\Jabatan;
use App\StatusPegawai;
use File;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pegawais = Pegawai::all();
        $golongans=Golongan::all();
        $jabatans=Jabatan::all();
        $status=StatusPegawai::all();
        return view ('pegawai.index', compact('pegawais', 'golongans', 'jabatans','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
        $golongans=Golongan::all();
        $jabatans=Jabatan::all();
        $status=StatusPegawai::all();
        return view('pegawai.create', compact('golongans','jabatans','status'));
=======
        $profiles = Profile::all();
        $golongans = Golongan::all();
        $jabatans = Jabatan::all();
        $status = StatusPegawai::all();
        return view('pegawai.create', compact('profiles', 'golongans', 'jabatans', 'status'));
>>>>>>> 7de9f445793010a5944920cda19e5c63b8dcefbf
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
        $this->validate($request,[
            'nama'=> 'required',
            'nip'=>'required',
            'nik'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'no_telpon'=>'required',
            'agama'=>'required',
            'status_nikah'=>'required',
            'foto'=>'mimes:jpeg,png,jpg|max:200',
            'golongan_id'=>'required',
            'jabatan_id'=>'required',
            'status_pegawai_id' => 'required'
=======
      
        
        $this->validate($request,[
            'nama_id'=>'required',
            'nip_id'=>'required',
            'nik_id'=>'required',
            'golongan_id'=>'required',
            'jabatan_id'=>'required',
            'status_pegawai_id'=>'required'
        ]);

        Pegawai::create([
            'nama_id'=>$request->nama_id,
            'nip_id'=>$request->nip_id,
            'nik_id'=>$request->nik_id,
            'golongan_id'=>$request->golongan_id,
            'jabatan_id'=>$request->jabatan_id,
            'status_pegawai_id'=>$request->status_pegawai_id
>>>>>>> 7de9f445793010a5944920cda19e5c63b8dcefbf
        ]);
        $gambar = $request->foto;
        $name_img = time(). ' - ' . $gambar->getClientOriginalName();

        Pegawai::create([
            'nama'=>$request->nama,
            'nip'=>$request->nip,
            'nik'=>$request->nik,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'tempat_lahir'=>$request->tempat_lahir,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'no_telpon'=>$request->no_telpon,
            'agama'=>$request->agama,
            'status_nikah'=>$request->status_nikah,
            'foto'=>$name_img,
            'golongan_id'=>$request->golongan_id,
            'jabatan_id'=>$request->jabatan_id,
            'status_pegawai_id'=>$request->status_pegawai_id
        ]);

        $gambar->move('images', $name_img);
            
        // $pegawais = $request->all();
        // Pegawai::create($pegawais);
        // if($request->hasFile('foto'))
        // {
        //     $gambar = $request->file('foto');
        //     $name_img = time(). '-' . $gambar->getClientOriginalName();
        //     $gambar->move('images',$name_img);
        // }

        return redirect('/pegawai');
    }


    /**
     * Display the specified resource.
     *
     * @param  App\Pegawai $pegawai
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pegawais = Pegawai::find($id);
        return view('pegawai.show', compact('pegawais'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Pegawai $pegawai
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $golongans=Golongan::all();
        $jabatans=Jabatan::all();
        $status=StatusPegawai::all();
        $pegawais = Pegawai::findorfail($id);
        $path ="images";
        return view('pegawai.edit', compact('pegawais','golongans','jabatans','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Pegawai $pegawai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
<<<<<<< HEAD
             $request->validate([
                  'nama'=>'required',
                     'nip'=>'required',
                  'nik'=>'required',
                  'jenis_kelamin'=>'required',
                  'tempat_lahir'=>'required',
                  'tanggal_lahir'=>'required',
                  'no_telpon'=>'required',
                  'agama'=>'required',
                  'status_nikah'=>'required',
                  'alamat'=>'required',
                  'foto'=>'mimes:jpeg,jpg,png|max:150',
                  'golongan_id'=>'required',
                  'jabatan_id'=>'required',
                  'status_pegawai_id'=>'required'
              ]);

         $pegawais = Pegawai::findorfail($id);
         if($request->has('foto')){
             $path="images";
             File::delete($path . $pegawais->foto);
             $gambar=$request->foto;
             $name_img=time(). ' - ' . $gambar->getClientOriginalName();
            $gambar->move('images', $name_img);
             $data_pegawai=[
                 'nama'=>$request->nama,
                 'nip'=>$request->nip,
                 'nik'=>$request->nik,
                 'jenis_kelamin'=>$request->jenis_kelamin,
                 'tempat_lahir'=>$request->tempat_lahir,
                 'tanggal_lahir'=>$request->tanggal_lahir,
                 'no_telpon'=>$request->no_telpon,
                 'agama'=>$request->agama,
                 'status_nikah'=>$request->status_nikah,
                 'alamat'=>$request->alamat,
                 'foto'=>$name_img,
                 'golongan_id'=>$request->golongan_id,
                 'jabatan_id'=>$request->jabatan_id,
                 'status_pegawai_id'=>$request->status_pegawai_id
             ];
         }else{
             $data_pegawai=[
                 'nama'=>$request->nama,
                 'nip'=>$request->nip,
                 'nik'=>$request->nik,
                 'jenis_kelamin'=>$request->jenis_kelamin,
                 'tempat_lahir'=>$request->tempat_lahir,
                 'tanggal_lahir'=>$request->tanggal_lahir,
                 'no_telpon'=>$request->no_telpon,
                 'agama'=>$request->agama,
                 'status_nikah'=>$request->status_nikah,
                 'alamat'=>$request->alamat,
                 'golongan_id'=>$request->golongan_id,
                 'jabatan_id'=>$request->jabatan_id,
                 'status_pegawai_id'=>$request->status_pegawai_id
             ];
         }

        $pegawais->update($data_pegawai);
        // $pegawais = Pegawai::find($id);
        // $pegawais ->update($request->all());
        // if ($request -> hasFile('foto')){
        //     $request -> file('foto') -> move('images',$request -> file('foto') -> getClientOriginalName());
        //     $pegawais -> foto = $request -> file('foto') -> getClientOriginalName();
        //     $pegawais -> save();
        // }
        return redirect('pegawai');
=======
        $request->validate([
            'nama'=>'required',
            'nip'=>'required',
            'nik'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'no_telpon'=>'required',
            'agama'=>'required',
            'status_nikah'=>'required',
            'alamat'=>'required',
        ]);

        $pegawais=Pegawai::where('id', $id)->update([
            'nama'=>$request->required,
            'nip'=>$request->required,
            'nik'=>$request->required,
            'jenis_kelamin'=>$request->required,
            'tempat_lahir'=>$request->required,
            'tanggal_lahir'=>$request->required,
            'no_telpon'=>$request->required,
            'agama'=>$request->required,
            'status_nikah'=>$request->required,
            'alamat'=>$request->required,
        ]);
        return view('/pegawai', compact('pegawais'));
>>>>>>> 7de9f445793010a5944920cda19e5c63b8dcefbf
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Pegawai $pegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::findorfail($id);
        $pegawai->delete();
        $path = "images/";
        File::delete($path.$pegawai->foto);

        return redirect('pegawai');
    }

    public function gaji($id)
    {
        $pegawais = Pegawai::find($id);
        return view('pegawai.gaji', compact('pegawais'));
    }
}
