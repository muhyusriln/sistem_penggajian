<?php

namespace App\Http\Controllers;

use App\Golongan;
use Illuminate\Http\Request;

class GolonganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $golongans = Golongan::all();
        return view('golongan.index', compact('golongans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('golongan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request-> validate([
            'nama'=>'required',
            'gaji_pokok'=>'required',
            'tunjangan_transport'=>'required',
            'tunjangan_makan'=>'required'
        ]);

        $golongans = $request->all();
        Golongan::create($golongans);
        return redirect('/golongan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Golongan  $golongan
     * @return \Illuminate\Http\Response
     */
    public function show(Golongan $golongan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Golongan  $golongan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $golongans = Golongan::find($id);
        return view('golongan.edit', compact('golongans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Golongan  $golongan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request-> validate([
            'nama'=>'required',
            'gaji_pokok'=>'required',
            'tunjangan_transport'=>'required',
            'tunjangan_makan'=>'required'
        ]);

        $golongans = Golongan::where('id',$id)->update([
            'nama'=> $request->nama,
            'gaji_pokok'=>$request->gaji_pokok,
            'tunjangan_transport'=>$request->tunjangan_transport,
            'tunjangan_makan'=>$request->tunjangan_makan,
        ]);
        return redirect('/golongan', compact('golongans'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Golongan  $golongan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $golongans = Golongan::where('id',$id)->delete();
        return redirect('/golongan', compact('golongans'));
    }
}
