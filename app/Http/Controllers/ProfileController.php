<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Profile;
use App\Golongan;
use App\Jabatan;
use App\StatusPegawai;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::all();
        return view ('profile.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
      return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama'=>'required',
            'nip'=>'required',
            'nik'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'no_telpon'=>'required',
            'agama'=>'required',
            'status_nikah'=>'required',
            'alamat'=>'required',
            'foto'=>'required|mimes:jpeg,png,jpg',
        
        ]);

        $gambar = $request->foto;
        $name_img = time(). '-' . $gambar->getClientOriginalName();
        
        Profile::create([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'nik' => $request->nik,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_telpon' => $request->no_telpon,
            'agama' => $request->agama,
            'status_nikah' => $request->status_nikah,
            'alamat' => $request->alamat,
            'foto' => $name_img,
          
        ]);
        $gambar->move('img', $name_img);
        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::findorfail($id);
        return view('profile.show', compact('profile'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::findorfail($id);
        return view('profile.edit', compact ('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama'=>'required',
            'nip'=>'required',
            'nik'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'no_telpon'=>'required',
            'agama'=>'required',
            'status_nikah'=>'required',
            'alamat'=>'required',
            'foto'=>'mimes:jpeg,png,jpg'
        ]);

        // $profiles= Profile::where('id', $id)->update([
        //     'nama'=>$request->required,
        //     'nip'=>$request->nip,
        //     'nik'=>$request->nik,
        //     'jenis_kelamin'=>$request->jenis_kelamin,
        //     'tempat_lahir'=>$request->tempat_lahir,
        //     'tanggal_lahir'=>$request->tanggal_lahir,
        //     'no_telpon'=>$request->no_telpon,
        //     'agama'=>$request->agama,
        //     'status_nikah'=>$request->status_nikah,
        //     'alamat'=>$request->alamat,
        //     'foto'=> $new_gambar
        // ]);
    

        $profile = Profile::findorfail($id);

        if ($request->has('foto')){
            $path = "/profiles";
            File::delete($path . $profile->foto);
            $gambar = $request->foto;
            $new_gambar = time(). ' - ' . $gambar->getClientOriginalName();
            $gambar->move('img/' , $new_gambar);
            $profile_data = [
                'nama'=>$request->required,
                'nip'=>$request->nip,
                'nik'=>$request->nik,
                'jenis_kelamin'=>$request->jenis_kelamin,
                'tempat_lahir'=>$request->tempat_lahir,
                'tanggal_lahir'=>$request->tanggal_lahir,
                'no_telpon'=>$request->no_telpon,
                'agama'=>$request->agama,
                'status_nikah'=>$request->status_nikah,
                'alamat'=>$request->alamat,
                'foto'=> $new_gambar
            ];
        }else{
            $profile_data = [
                'nama'=>$request->required,
                'nip'=>$request->nip,
                'nik'=>$request->nik,
                'jenis_kelamin'=>$request->jenis_kelamin,
                'tempat_lahir'=>$request->tempat_lahir,
                'tanggal_lahir'=>$request->tanggal_lahir,
                'no_telpon'=>$request->no_telpon,
                'agama'=>$request->agama,
                'status_nikah'=>$request->status_nikah,
                'alamat'=>$request->alamat
           
            ];
        }

        $profile->update($profile_data);
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::findorfail($id);
        $profile->delete();

        $path = "img/";
        File::delete($path . $profile->foto);

        return redirect('/profile');




        // $profiles = Profile::where('id',$id)->delete();
        // return redirect('/profile', compact('pegawais'));
    }
}
