<?php

namespace App\Http\Controllers;

use App\StatusPegawai;
use Illuminate\Http\Request;

class StatusPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = StatusPegawai::all();
        return view('status_pegawai.index', compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('status_pegawai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status'=>'required',
            'gaji_diterima'=>'required'
        ]);

        $status = $request->all();
        StatusPegawai::create($status);
        return redirect('/status_pegawai');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatusPegawai  $statusPegawai
     * @return \Illuminate\Http\Response
     */
    public function show(StatusPegawai $statusPegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatusPegawai  $statusPegawai
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = StatusPegawai::find($id);
        return view('status_pegawai.edit',compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatusPegawai  $statusPegawai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status'=>'required',
            'gaji_diterima'=>'required'
        ]);

        $status = StatusPegawai::where('id',$id)->update([
            'status'=> $request->required,
            'gaji_diterima'=>$request->required,
        ]);

        return view('status_pegawai', compact('status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatusPegawai  $statusPegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = StatusPegawai::where('id',$id)->delete();
        return redirect('/status_pegawai', compact('status'));
    }
}
