<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $guarded =['id'];

    public function pegawai()
    {
        return $this->hasMany('App\Pegawai');
    }
}
