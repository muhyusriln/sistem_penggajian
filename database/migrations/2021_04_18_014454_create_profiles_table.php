<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->integer('nip');
            $table->integer('nik');
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->integer('no_telpon');
            $table->string('agama');
            $table->string('status_nikah');
            $table->longText('alamat');
            $table->string('foto');
<<<<<<< HEAD:database/migrations/2021_04_16_235017_create_pegawais_table.php
            $table->unsignedBigInteger('golongan_id')->foreign('golongan_id')->references('id')->on('golongans');
            $table->unsignedBigInteger('status_pegawai_id')->foreign('status_pegawai_id')->references('id')->on('status_pegawais');
            $table->unsignedBigInteger('jabatan_id')->foreign('jabatan_id')->references('id')->on('jabatans');
=======
           

>>>>>>> 7de9f445793010a5944920cda19e5c63b8dcefbf:database/migrations/2021_04_18_014454_create_profiles_table.php
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
<<<<<<< HEAD:database/migrations/2021_04_16_235017_create_pegawais_table.php
        Schema::dropIfExists('pegawais');
=======
        Schema::dropIfExists('profiles');
       
>>>>>>> 7de9f445793010a5944920cda19e5c63b8dcefbf:database/migrations/2021_04_18_014454_create_profiles_table.php
    }
}
