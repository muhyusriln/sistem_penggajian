@extends('layouts.master')

@section('title')
    Silakan Tambah Data
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-header">
            <a href="/pegawai"  type="button" class="btn btn-warning">Kembali</a>
          </div>
        <h5> Form Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="post" action="/pegawai" enctype="multipart/form-data">
        @csrf
<<<<<<< HEAD
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">NAMA PEGAWAI</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nama">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">NIP</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" name="nip" id="nip">
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">NIK</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" name="nik" id="nik">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">JENIS KELAMIN</label>
            <div class="col-sm-10">
                <select type="enumerik" class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                    <option>---Pilih Jenis Kelamin---</option>
                    <option value="Laki-Laki" {{old('jenis_kelamin') == 'Laki-Laki'  ? 'selected' : ''}}>Laki-Laki</option>
                    <option value="Perempuan" {{old('jenis_kelamin') == 'Perempuan'  ? 'selected' : ''}}>Perempuan</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">TEMPAT LAHIR</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">TANGGAL LAHIR</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">NO TELEPON</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" name="no_telpon" id="no_telpon">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">AGAMA</label>
            <div class="col-sm-10">
                <select type="enumerik" class="form-control" name="agama" id="agama">
                    <option>---Pilih agama---</option>
                    <option>Kristen</option>
                    <option>Islam</option>
                    <option>Katholik</option>
                    <option>Budha</option>
                    <option>Hindu</option>
                    <option>Khonghucu</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">STATUS MENIKAH</label>
            <div class="col-sm-10">
                <select type="enumerik" class="form-control" name="status_nikah" id="status_nikah">
                    <option>---Pilih Status Menikah---</option>
                    <option>Sudah Menikah</option>
                    <option>Belum Menikah</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">ALAMAT</label>
            <div class="col-sm-10">
                <textarea rows="5" class="form-control" name="alamat" id="alamat"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">FOTO</label>
            <div class="col-sm-10">
                <div class="custom-file">
                    <label type="file" for="foto" class="custom-file-label">
                    <input type="file" class="form-control" name="foto" id="foto">
                    </label> 
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">GOLONGAN</label>
            <div class="col-sm-10">
                    <select id="golongan_id" name="golongan_id" class="form-control">>
                        <option>---Pilih Golongan---</option>
                                @foreach ($golongans as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                    </select>
            </div>
        </div>
        <br>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">JABATAN</label>
            <div class="col-sm-10">
                    <select id="jabatan_id" name="jabatan_id" class="form-control">>
                        <option>---Pilih Jabatan---</option>
                                @foreach ($jabatans as $item)
                                    <option value="{{ $item->id }}">{{ $item->jabatan }}</option>
                                @endforeach
                    </select>
            </div>
        </div>
        <br>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">STATUS PEGAWAI</label>
            <div class="col-sm-10">
                    <select id="status_pegawai_id" name="status_pegawai_id" class="form-control">>
                        <option>---Pilih Status Pegawai---</option>
                                @foreach ($status as $item)
                                    <option value="{{ $item->id }}">{{ $item->status }}</option>
                                @endforeach
                    </select>
            </div>
        </div>
=======
            @foreach ($profiles as $profile)
                
            
            <div class="form-group form-default">
                <span class="form-bar"></span>
                <label class="float-label"></label>
                <select name="nama_id" class="form-control">
                    <option value="">Nama Pegawai</option>
                        <option value="{{$profile->id}}">{{$profile->nama}}</option>

                </select>
            </div>
            <div class="form-group form-default">
                <span class="form-bar"></span>
                <label class="float-label"></label>
                <select name="nip_id" class="form-control">
                    <option value="">NIP</option>
                        <option value="{{$profile->id}}">{{$profile->nip}}</option>

                </select>
            </div>
           
            <div class="form-group form-default">
                <span class="form-bar"></span>
                <label class="float-label"></label>
                <select name="nik_id" class="form-control">
                    <option value="">NIK</option>
                        <option value="{{$profile->id}}">{{$profile->nik}}</option>

                </select>
            </div>
         @endforeach

            <div class="form-group form-default">
                <span class="form-bar"></span>
                <label class="float-label"></label>
                <select name="golongan_id" class="form-control">
                    <option value="">Golongan</option>
                    @foreach ($golongans as $golongan)
                        <option value="{{$golongan->id}}">{{$golongan->nama}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group form-default">
                <span class="form-bar"></span>
                <label class="float-label"></label>
                <select name="jabatan_id" class="form-control">
                    <option value="">Jabatan</option>
                    @foreach ($jabatans as $jabatan)
                        <option value="{{$jabatan->id}}">{{$jabatan->jabatan}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group form-default">
                <span class="form-bar"></span>
                <label class="float-label"></label>
                <select name="status_pegawai_id" class="form-control">
                    <option value="">Status Pegawai</option>
                    @foreach ($status as $statusp)
                        <option value="{{$statusp->id}}">{{$statusp->status}}</option>
                    @endforeach
                </select>
            </div>
            
           
>>>>>>> 7de9f445793010a5944920cda19e5c63b8dcefbf
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
    
@endsection