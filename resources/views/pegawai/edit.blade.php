@extends('layouts.master')

@section('title')
    Edit Data Pegawai
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h5>Form Edit Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="POST" action="pegawai/{{ $pegawais->id }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NAMA PEGAWAI</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" value="{{ $pegawais->nama}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="nip" id="nip" value="{{ $pegawais->nip }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NIK</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="nik" id="nik" value="{{ $pegawais->nik }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">JENIS KELAMIN</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">TEMPAT LAHIR</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="{{ $pegawais->tempat_lahir }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">TANGGAL LAHIR</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="{{ $pegawais->tanggal_lahir }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NO TELEPON</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="no_telpon" id="no_telpon" value="{{ $pegawais->no_telpon }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">AGAMA</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="agama" id="agama">
                              >
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">STATUS MENIKAH</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="status_nikah" id="status_nikah">
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">ALAMAT</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="alamat" id="alamat">{{ $pegawais->alamat }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">FOTO</label>
                        <div class="col-sm-10">
                            <div class="custom-file">
                                <label type="file" for="foto" class="custom-file-label">
                                <input type="file" class="form-control" name="foto" id="foto">
                                </label> 
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="foto" id="foto"><>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">GOLONGAN</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="golongan_id " id="golongan_id " >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">JABATAN</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="jabatan_id " id="jabatan_id" ">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">STATUS PEGAWAI</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="status_pegawai_id " id="status_pegawai_id" }">
                        </div>
                    </div>
                    
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="pegawai" class="btn btn-warning">Batal</a>
            </div>
            
            
        </form>
    </div>
</div>
    
@endsection