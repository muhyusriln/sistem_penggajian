@extends('layouts.master')

@section('title')
    Edit Data Pegawai
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-header">
            <a href="/pegawai"  type="button" class="btn btn-warning">Kembali</a>
          </div>
        <h5>l Form Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="post" action="/pegawai">
        @csrf

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Pegawai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" value="{{ $pegawais->nama}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="nip" id="nip" value="{{ $pegawais->nip }}">
                        </div>
                    </div>
                    
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/golongan" class="btn btn-warning">Batal</a>
            </div>
            
            
        </form>
    </div>
</div>
    
@endsection