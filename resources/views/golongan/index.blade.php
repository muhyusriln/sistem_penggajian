@extends('layouts.master')

@section('title')
Golongan Pegawai
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <a href="/golongan/create"  type="button" class="btn btn-success">Tambah Data Golongan</a>
    
    </div>
<div class="card-block table-border-style">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Golongan</th>
                    <th>Gaji Pokok</th>
                    <th>Tunjangan Transport</th>
                    <th>Tunjangan Makan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($golongans as $key=>$value)
                    <tr>
                        <td scope="row">{{ $key + 1 }}</td>
                        <td>{{ $value->nama }}</td>
                        <td>@currency($value->gaji_pokok)</td>
                        <td>@currency($value->tunjangan_transport)</td>
                        <td>@currency($value->tunjangan_makan)</td>
                        <td>
                            <a href="/golongan/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                            <form action="/golongan/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
            @empty
                
            @endforelse
                
            </tbody>
        </table>
    </div>
</div>
@endsection
