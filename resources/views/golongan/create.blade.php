@extends('layouts.master')

@section('title')
    Silakan Tambah Data
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-header">
            <a href="/golongan"  type="button" class="btn btn-warning">Kembali</a>
          </div>
        <h5>l Form Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="post" action="/golongan">
        @csrf
            <div class="form-group form-default">
                <input type="text" name="nama" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Nama Golongan</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="gaji_pokok" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Gaji Pokok</label>
            </div>
           
            <div class="form-group form-default">
                <input type="numerikt" name="tunjangan_transport" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Tunjangan Transpor</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="tunjangan_makan" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Tunjangan Makan</label>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
    
@endsection