@extends('layouts.master')

@section('title')
    Edit Data
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h5>Form Edit Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="POST" action="/golongan/{{ $golongans->id }}">
        @csrf
        @method('PUT')

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Golongan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" value="{{ $golongans->nama}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Gaji Pokok</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="gaji_pokok" id="gaji_pokok" placeholder="Type your title in Placeholder" value="{{ $golongans->gaji_pokok }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tunjangan Transport</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="tunjangan_transport" id="tunjangan_transport" placeholder="Type your title in Placeholder" value="{{ $golongans->tunjangan_transport }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tunjangan Makan</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="tunjangan_makan" id="tunjangan_makan" placeholder="Password input" value="{{ $golongans->tunjangan_makan}}">
                        </div>
                    </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/golongan" class="btn btn-warning">Batal</a>
            </div>
            
            
        </form>
    </div>
</div>
    
@endsection