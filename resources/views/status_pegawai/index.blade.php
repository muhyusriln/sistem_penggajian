@extends('layouts.master')

@section('title')
Data Status Kepegawaian
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <a href="/status_pegawai/create"  type="button" class="btn btn-success">Tambah Data Status Kepegawaian</a>
      
    </div>
<div class="card-block table-border-style">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Status Kepegawaian</th>
                    <th>Persentase Gaji Diterima</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($status as $key=>$value)
                    <tr>
                        <td scope="row">{{ $key + 1 }}</td>
                        <td>{{ $value->status}}</td>
                        <td>{{ $value->gaji_diterima }}%</td>
                        <td>
                            <a href="/status_pegawai/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                            <form action="/status_pegawai/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
            @empty
                
            @endforelse
                
            </tbody>
        </table>
    </div>
</div>
@endsection
