@extends('layouts.master')

@section('title')
    Silakan Tambah Status Kepegawaian
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-header">
            <a href="/status_pegawai"  type="button" class="btn btn-warning">Kembali</a>
          </div>
        <h5>Form Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="post" action="/status_pegawai">
        @csrf
            <div class="form-group form-default">
                <input type="text" name="status" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Status Kepegawaian</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="gaji_diterima" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Persentase Gaji Diterima</label>
            </div>
           
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
    
@endsection