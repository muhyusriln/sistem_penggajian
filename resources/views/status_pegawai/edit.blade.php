@extends('layouts.master')

@section('title')
    Edit Data Status Kepegawaian
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h5>Form Edit Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="POST" action="/status_pegawai/{{ $status->id }}">
        @csrf
        @method('PUT')

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status Kepegawaian</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="status" value="{{ $status->status}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Persentase Gaji Diterima</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="gaji_diterima" id="gaji_diterima" value="{{ $status->gaji_diterima }}">
                        </div>
                    </div>
                    
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/golongan" class="btn btn-warning">Batal</a>
            </div>
            
            
        </form>
    </div>
</div>
    
@endsection