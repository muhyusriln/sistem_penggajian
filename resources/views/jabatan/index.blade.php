@extends('layouts.master')

@section('title')
Data Jabatan
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <a href="/jabatan/create"  type="button" class="btn btn-success">Tambah Data Jabatan</a>

    </div>
<div class="card-block table-border-style">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Jabatan</th>
                    <th>Tunjangan Jabatan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($jabatans as $key=>$value)
                    <tr>
                        <td scope="row">{{ $key + 1 }}</td>
                        <td>{{ $value->jabatan }}</td>
                        <td>@currency($value->tunjangan_jabatan)</td>
                        <td>
                            <a href="/jabatan/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                            <form action="/jabatan/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
            @empty
                
            @endforelse
                
            </tbody>
        </table>
    </div>
</div>
@endsection
