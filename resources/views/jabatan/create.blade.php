@extends('layouts.master')

@section('title')
    Silakan Tambah Data
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-header">
            <a href="/jabatan"  type="button" class="btn btn-warning">Kembali</a>
          </div>
        <h5>Form Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="post" action="/jabatan">
        @csrf
            <div class="form-group form-default">
                <input type="text" name="jabatan" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Nama Jabatan</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="tunjangan_jabatan" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Tunjangan Jabatan</label>
            </div>
           
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
    
@endsection