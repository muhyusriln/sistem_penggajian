@extends('layouts.master')

@section('title')
    Edit Data Jabatan
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h5>Form Edit Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="POST" action="/jabatan/{{ $jabatans->id }}">
        @csrf
        @method('PUT')

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Jabatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="jabatan" value="{{ $jabatans->jabatan}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tunjangan Jabatan</label>
                        <div class="col-sm-10">
                            <input type="numerik" class="form-control" name="tunjangan_jabatan" id="tunjangan_jabatan" value="{{ $jabatans->tunjangan_jabatan }}">
                        </div>
                    </div>
                    
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/golongan" class="btn btn-warning">Batal</a>
            </div>
            
            
        </form>
    </div>
</div>
    
@endsection