@extends('layouts.master')

@section('title')
    Silakan Tambah Profile Pegawai
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-header">
            <a href="/profile"  type="button" class="btn btn-warning">Kembali</a>
          </div>
        <h5>Form Inputs</h5>
        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
    </div>
    <div class="card-block">
        <form class="form-material" method="post" action="/profile" enctype="multipart/form-data">
        @csrf
            <div class="form-group form-default">
                <input type="text" name="nama" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Nama</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="nip" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">NIP</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="nik" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">NIK</label>
            </div>
            <div class="form-group form-default">
                <input type="text" name="jenis_kelamin" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Jenis Kelamin</label>
            </div>
            <div class="form-group form-default">
                <input type="text" name="tempat_lahir" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Tempat Lahir</label>
            </div>
            <div class="form-group form-default">
                <input type="date" name="tanggal_lahir" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Tanggal Lahir</label>
            </div>
            <div class="form-group form-default">
                <input type="numerik" name="no_telpon" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">No Telepon</label>
            </div>
            <div class="form-group form-default">
                <input type="text" name="agama" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Agama</label>
            </div>
            <div class="form-group form-default">
                <input type="text" name="status_nikah" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Status Menikah</label>
            </div>
            <div class="form-group form-default">
                <input type="text" name="alamat" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Alamat</label>
            </div>
            <div class="form-group form-default">
                <input type="file" name="foto" class="form-control">
                <span class="form-bar"></span>
                <label class="float-label">Foto</label>
            </div>

            
           
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
    
@endsection