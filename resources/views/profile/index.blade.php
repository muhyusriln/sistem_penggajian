@extends('layouts.master')

@section('title')
Data Profile Pegawai
@endsection
@section('content')
<div class="card">
    <div class="card-header">
      <a href="/profile/create"  type="button" class="btn btn-success">Tambah Data Profile Pegawai</a>

    </div>
<div class="card-block table-border-style">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>NIK</th>
                    <th>Golongan</th>
                    <th>Jabatan</th>
                    <th>Status Pegawai</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($profiles as $key=> $value)
                    <tr>
                        <td scope="row">{{ $key + 1 }}</td>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->nip}}</td>
                        <td>{{$value->nik}}</td>
                        <td>{{$value->jenis_kelamin}}</td>
                        <td>{{$value->tempat_lahir}}</td>
                        <td>{{$value->tanggal_lahir}}</td>
                        <td>
                            <a href="/profile/{{ $value->id }}" class="btn btn-info">Show</a>
                            <a href="/profile/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                            <form action="/profile/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
            @endforeach
                    
                
           
                
            </tbody>
        </table>
    </div>
</div>
@endsection